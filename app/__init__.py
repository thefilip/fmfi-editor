import os

from flask            import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login      import LoginManager
from flask_bcrypt     import Bcrypt
from flask_migrate import Migrate

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config.from_object('app.config.Config')

db = SQLAlchemy  (app) # flask-sqlalchemy
bc = Bcrypt      (app) # flask-bcrypt
migrate = Migrate(app, db)


lm = LoginManager(   ) # flask-loginmanager
lm.init_app(app)       # init the login manager

# Setup database

def initialize_database():
    db.create_all()



# Import routing, models and Start the App
from app import views, models


@lm.user_loader
def load_user(user_id):
    return models.Users.query.get(int(user_id))