let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

canvas.width = window.innerWidth-20;
canvas.height = window.innerHeight-100;

canvas.style.border = '5px solid blue'

class Point{
    constructor(x,y) {
        this.x = x
        this.y = y
        this.R = 5
    }

    x(){
        return this.x
    }
    y(){
        return this.y
    }

    draw(ctx){
        ctx.beginPath();
        ctx.arc(this.x-(this.R*2),this.y-this.R,this.R,0,Math.PI*2);
        ctx.strokeStyle = 'black'
        ctx.lineWidth = 3
        ctx.fillStyle = 'black'
        ctx.fill()
        ctx.stroke();
        ctx.closePath();
    }
    erase(ctx){
        ctx.clearRect(this.x-(this.R*Math.PI*2),this.y-(this.R*Math.PI*2),this.x+this.R,this.y+this.R)
    }

}

class Polygon{
    constructor(points,color) {
        this.points = points
        this.color = color
    }

    draw(ctx){
        ctx.beginPath()
        ctx.lineWidth = 2;
        ctx.strokeStyle = this.color;
        let l = this.points.length
        for (let i = 0; i < l; i++) {
            console.log(i)
            this.points[i%l].erase(ctx)
            ctx.lineTo(this.points[i%l].x,this.points[i%l].y)
            ctx.lineTo(this.points[(i+1)%l].x,this.points[(i+1)%l].y)
        }
        ctx.stroke()
    }
}
points = []
canvas.addEventListener('click', (event)=>{
    const rect = canvas.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top
    const p = new Point(x,y)
    p.draw(context)
    points.push(p)
    console.log("x: " + x + " y: " + y)
    console.log(points)
})


document.addEventListener('keypress', function (e) {
    console.log("aaaaa")
    if (e.key === 'Enter') {
        // Your code to handle the 'Enter' key press goes here
        const randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
        let pol  = new Polygon(points,randomColor)
        console.log("drawing: "+points)
        pol.draw(context)
        points.length = 0

    }
});

