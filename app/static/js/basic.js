
function moveFunction() {
    console.log("moveFunction")
    const canvas = document.getElementById("planCanvas");
    if (canvas.getContext) {
    const ctx = canvas.getContext("2d");

    ctx.fillRect(25, 25, 100, 100);
    ctx.clearRect(45, 45, 60, 60);
    ctx.strokeRect(50, 50, 50, 50);
  }

}

function makeCanvas(){
    var canvas = document.getElementById("myCanvas");

    // Get the rendering context
    var ctx = canvas.getContext("2d");
    var width = 1600;
    var height = 900;

    canvas.width = width * window.devicePixelRatio;
    canvas.height = height * window.devicePixelRatio;

    // Scale the drawing context to match the display density
    ctx.scale(window.devicePixelRatio, window.devicePixelRatio);

}
function toolbarClickFunc(element) {
    console.log(element.title)
    const canvas = document.getElementById("planCanvas");
    if (canvas.getContext) {
        const ctx = canvas.getContext("2d");
        ctx.width = window.innerWidth
        ctx.height= window.innerHeight
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        // Set the font and text properties
        ctx.font = "30px Arial";
        ctx.fillStyle = "blue";
        ctx.textAlign = "center";

        // Draw text on the canvas
        ctx.fillText(element.title, canvas.width / 2, canvas.height / 2);
    }
}
function zoomFunction() {
    console.log("zoomFunction")
}
function floorUpFunction() {
    console.log("floorUpFunction")
}
function floorDownFunction() {
    console.log("floorDownFunction")
}
function roomFunction() {
    console.log("roomFunction")
}
function doorFunction() {
    console.log("doorFunction")
}
function stairsFunction() {
    console.log("stairsFunction")
}
function objectsFunction() {
    console.log("objectsFunction")
}
function customObjectsFunction() {
    console.log("customObjectsFunction")
}